#include <iostream>
#include <string>

// Running the main function
int main()
{
  // Creating an UNSIGNED VARIABLE
  unsigned int Age = 27;              // Creating a non-negative value

  // Displaying the created unsigned variable.
  std::cout << "My age is: " << Age << std::endl;


  // Closing the program.
  return 0;
}
