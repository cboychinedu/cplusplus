#include <iostream>
#include <string>

// Running the main function
int main()
{
// Creating a for-loop
for (int i = 0; i <= 10; i++)
{
  std::cout<< "The first value for i is: " << i << std::endl;
}

// Closing the program
return 0;

}
