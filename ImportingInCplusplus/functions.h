// Creating a module that would be called upon into the main
// function for calculations operations
// Importing the required modules
#include <iostream>
#include <string>


// Creating a function for performing addition
double sum_numbers(double num1, double num2) {
  // Adding both num1 and num2
  return num1 + num2;
}

// Creating a function for performing substraction
double subtract_numbers(double num1, double num2) {
  // Subtracting num2 from num1
  return num1 - num2;
}

// Creating a function for performing division
double divide_numbers(double num1, double num2) {
  // Divide num1 by num2
  return num1 / num2;
}

// Creating a function for performing multiplication
