// Importing the necessary modules
#include <iostream>
#include <string>

// Importing created external modules
#include "functions.h"

// Using the standard namespace lib
using std::cout;

// Running the "main" function
int main() {
    // Declaring some specific variables
    std::string value_specified;
    double num1, num2, result;

    // Assigning the numbers some specific values
    num1 = 100;
    num2 = 24.56;

    // Adding both numbers
    result = sum_numbers(num1, num2);

    // Displaying the results
    std::cout << "The result for the addition of "
              << num1 << " and " << num2 << " is " << result
              << std::endl; 

    // Closing the function
    return 0;
}
