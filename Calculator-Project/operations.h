
// Including the header file
#include <iostream>


/* Creating multiple functions for the calculator process */
// Addition function
double addition(double num1, double num2)
{
  // Performing the calculation
  return num1 + num2;

}


// Subtraction function
double subtraction(double num1, double num2)
{
  // Performing the calculation
  return num1 - num2;

}

// Division function
double division(double num1, double num2)
{
  // Performing the calculation
  return num1 / num2;

}

// Multiplication function
double multiplication(double num1, double num2)
{
  // Performing the calculation
  return num1 * num2;
}
