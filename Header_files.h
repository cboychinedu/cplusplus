// Including the header files
#include <iostream>
#include <cmath>

// Defining a function
int sumOfTwoNumbers(int a, int b)
{
  // Displaying something firstly before calculation
  std::cout << "Calculating.........." << std::endl;
  
  // returning the result
  return (a + b);
}
