// Incuding the header files
#include <iostream>
#include <string>
#include <vector>

// Running the main function
int main()
{
    // Using auto declaration
    auto int_var = 32;

    std::cout << "Double: " << int_var << std::endl;

    // Closing the program
    return 0;
}
