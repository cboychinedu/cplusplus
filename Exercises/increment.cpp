// Including the header files
#include <iostream>
#include <string>

// Running the main function
int main()
{
    // Creating variables of specific integer values.
    int a = 20;
    int b = 20;

    // Displaying some values
    std::cout << "a = " << a << ":  b = " << b << "\n";

    // Using the increment operator
    std::cout << "a++ = " << a++ << ": ++b = " << ++b << "\n";

    // Displaying the values
    std::cout << "a = " << a << ":  b = " << b << "\n";

    // Closing the program
    return 0;
}
